#include <stdio.h>
#include <stdint.h>
#include <libmalt.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <progressor/malt_progressor.h>
#include <progressor_memory_ext.h>

#include "../common/measurements_common.h"


#define GLOBAL_TEST_DATA_BUFFER_SIZE  1000
#define NUM_POINTS_IN_MEASUREMENT     10
#define NUM_REPETITIONS               100


typedef struct{
    int hw_version_major;
    int hw_version_minor;
    int hw_version_revision;
    int rom_version_major;
    int rom_version_minor;
    int sdk_version_major;
    int sdk_version_minor;
    int sdk_version_revision;
    int spbus_frequency_khz;
    int global_memory_frequency_khz;
    int global_memory_size_bytes;
    char device_type[16];
    
    int slave_cores_num;
    int slave_lines_num;
    int slave_cores_per_line_num;
    int slave_memory_size_bytes;
    int slave_core_frequency_khz;
    long long int global_memory_to_slave_bandwidth_byte_per_s;
    long long int global_memory_to_slave_latency_ns;
    long long int global_memory_to_slave_block_bandwidth_byte_per_s;
    long long int global_memory_to_slave_block_latency_ns;
    long long int slave_to_global_memory_bandwidth_byte_per_s;
    long long int slave_to_global_memory_latency_ns;
    long long int slave_to_global_memory_block_bandwidth_byte_per_s;
    long long int slave_to_global_memory_block_latency_ns;
    
    char have_coprocessor;
    char coprocessor_type[16];
    int coprocessor_workers_num;
    int coprocessor_columns_num;
    int coprocessor_rows_num;
    int coprocessor_column_data_memory_size_bytes;
    int coprocessor_column_code_memory_size_bytes;
    int coprocessor_frequency_khz;
    long long int slave_to_coprocessor_bandwidth_byte_per_s;
    long long int slave_to_coprocessor_latency_ns;
    long long int coprocessor_to_slave_bandwidth_byte_per_s;
    long long int coprocessor_to_slave_latency_ns;
    long long int coprocessor_shift_bandwidth_byte_per_s;
    long long int coprocessor_shift_latency_ns;
} device_info_t;

device_info_t device_info;
// We use hardcoded addresses here to avoid internal on-chip memory placement
// 1st k by default is placed in on-chip memory.
//volatile uint8_t global_test_data_buffer[GLOBAL_TEST_DATA_BUFFER_SIZE] __attribute__ ((aligned (16)));
//uint8_t* const global_test_data_buffer = (uint8_t*)0x81000000; // LVDS address
uint8_t* const global_test_data_buffer = (uint8_t*)0xC1000000; // DDR address

/**********************   AUX functions   **********************/


void fill_basic_device_info(device_info_t *info)
{
    info->hw_version_major = malt_get_major_version();
    info->hw_version_minor = malt_get_minor_version();
    info->hw_version_revision = malt_get_revision_version();
    info->rom_version_major = malt_get_major_rom_version();
    info->rom_version_minor = malt_get_minor_rom_version();
    info->sdk_version_major = malt_get_major_sdk_version();
    info->sdk_version_minor = malt_get_minor_sdk_version();
    info->spbus_frequency_khz = malt_get_bus_freq();
    info->global_memory_frequency_khz = malt_get_mem_freq();
    info->global_memory_size_bytes = malt_get_shared_mem_size();
    
    if (malt_is_emu())
    {
        sprintf(info->device_type, "Emulator");
    }
    else if (malt_is_fpga())
    {
        sprintf(info->device_type, "FPGA");
    }
    else if (malt_is_pasic())
    {
        sprintf(info->device_type, "ASIC");
    }
    else
    {
        sprintf(info->device_type, "Unknown");
    }
    
    info->slave_cores_num = malt_get_total_slaves();
    info->slave_lines_num = malt_get_lines_count();
    info->slave_cores_per_line_num = malt_get_slaves_count();
    info->slave_memory_size_bytes = malt_get_local_mem_size();
    info->slave_core_frequency_khz = malt_get_core_freq();
    
    if (malt_coproc_present())
    {
        info->have_coprocessor = 1;

        if (malt_has_gepard_coproc())
        {
            sprintf(info->coprocessor_type, "Gepard");
        }
        else if (malt_has_progressor_coproc())
        {
            sprintf(info->coprocessor_type, "Progressor");
        }
        else
        {
            sprintf(info->coprocessor_type, "Unknown");
        }
        info->coprocessor_workers_num = malt_pgr_get_cluster_size();
        info->coprocessor_columns_num = malt_pgr_get_col_num();
        info->coprocessor_rows_num = malt_pgr_get_row_num();
        info->coprocessor_column_data_memory_size_bytes = malt_pgr_get_dmem_size() * 4;
        info->coprocessor_column_code_memory_size_bytes = malt_pgr_get_code_size() * 8;
        info->coprocessor_frequency_khz = malt_get_coproc_freq();
    }
    else
    {
        info->have_coprocessor = 0;
    }
    
}

void print_device_info(device_info_t *info)
{
    printf("############################ \n");
    printf("#       Device info        # \n");
    printf("############################ \n");

    printf("\n-------- General info --------\n");
    printf("Device type:             %s\n", info->device_type);
    printf("Hardware version:        %d.%d.%d\n", info->hw_version_major,
                                                  info->hw_version_minor,
                                                  info->hw_version_revision);
    printf("ROM version:             %d.%d\n", info->rom_version_major,
                                               info->rom_version_minor);
    printf("SDK version:             %d.%d\n", info->sdk_version_major,
                                               info->sdk_version_minor);
    printf("SPBUS frequency:         %d MHz\n", info->spbus_frequency_khz / 1000);
    printf("Global memory frequency: %d MHz\n", info->global_memory_frequency_khz / 1000);
    printf("Global memory size:      %d bytes\n", info->global_memory_size_bytes);
    
    printf("\n------ Slave cores info ------\n");
    printf("Total slave coresnumber: %d\n", info->slave_cores_num);
    printf("Lines:                   %d\n", info->slave_lines_num);
    printf("Slave cores per line:    %d\n", info->slave_cores_per_line_num);
    printf("Core frequency:          %d MHz\n", info->slave_core_frequency_khz / 1000);
    printf("Local memory size:       %d bytes\n",info->slave_memory_size_bytes);
    printf("Global memory to slave bandwidth:       %lld KBytes/s \n", info->global_memory_to_slave_bandwidth_byte_per_s);
    printf("Global memory to slave latency:         %lld ns \n", info->global_memory_to_slave_latency_ns);
    printf("Global memory to slave block bandwidth: %lld KBytes/s \n", info->global_memory_to_slave_block_bandwidth_byte_per_s);
    printf("Global memory to slave block latency:   %lld ns \n", info->global_memory_to_slave_block_latency_ns);
    printf("Slave to global memory bandwidth:       %lld KBytes/s \n", info->slave_to_global_memory_bandwidth_byte_per_s);
    printf("Slave to global memory latency:         %lld ns \n", info->slave_to_global_memory_latency_ns);
    printf("Slave to global memory block bandwidth: %lld KBytes/s \n", info->slave_to_global_memory_block_bandwidth_byte_per_s);
    printf("Slave to global memory block latency:   %lld ns \n", info->slave_to_global_memory_block_latency_ns);

    
    if (info->have_coprocessor)
    {
        printf("\n------ Coprocessor info ------\n");
        printf("Coprocessor type:        %s\n", info->coprocessor_type);
        printf("Number workers:          %d\n", info->coprocessor_workers_num);
        printf("Number of columns:       %d\n", info->coprocessor_columns_num);
        printf("Number of rows:          %d\n", info->coprocessor_rows_num);
        printf("Local memory block size: %d bytes\n", info->coprocessor_column_data_memory_size_bytes);
        printf("Column code memory size: %d bytes\n", info->coprocessor_column_code_memory_size_bytes);
        printf("Coprocessor frequency:   %d MHz\n", info->coprocessor_frequency_khz / 1000);
        printf("Slave to coprocessor bandwidth: %lld KBytes/s \n", info->slave_to_coprocessor_bandwidth_byte_per_s);
        printf("Slave to coprocessor latency:   %lld ns \n", info->slave_to_coprocessor_latency_ns);
        printf("Coprocessor to slave bandwidth: %lld KBytes/s \n", info->coprocessor_to_slave_bandwidth_byte_per_s);
        printf("Coprocessor to slave latency:   %lld ns \n", info->coprocessor_to_slave_latency_ns);
        printf("Coprocessor memory shift:       %lld ns \n", info->coprocessor_shift_latency_ns);
    }
    printf("\n");
}


/**********************   Test functions   **********************/

void test_global_memory_to_slave_bandwidth_slave(int data_size, int num_repetitions)
{
    volatile uint8_t local_test_data_buffer[GLOBAL_TEST_DATA_BUFFER_SIZE] __attribute__ ((aligned (16)));
    for (int i = 0; i < num_repetitions; i++)
    {
        malt_memcpy((void *)local_test_data_buffer, (void *)global_test_data_buffer, data_size * sizeof(uint8_t));
    }
}

void test_global_memory_to_slave_bandwidth(device_info_t *info)
{
    printf("Test block reading from global to local memory\n");
    bandwidth_measurement_t m;
    int size_step = (GLOBAL_TEST_DATA_BUFFER_SIZE - 1) / NUM_POINTS_IN_MEASUREMENT;
    m = make_standard_measurement(test_global_memory_to_slave_bandwidth_slave,
                                  size_step,
                                  NUM_POINTS_IN_MEASUREMENT,
                                  NUM_REPETITIONS);
    print_measurement(m);
    info->global_memory_to_slave_block_bandwidth_byte_per_s = m.bandwidth;
    info->global_memory_to_slave_block_latency_ns = m.latency_ns;
}


void test_serial_global_memory_to_slave_bandwidth_slave(int data_size, int num_repetitions)
{
    #pragma GCC diagnostic ignored "-Wunused-but-set-variable"
    volatile uint8_t local_test_data_buffer[GLOBAL_TEST_DATA_BUFFER_SIZE] __attribute__ ((aligned (16)));
    #pragma GCC diagnostic pop
    
    for (int i = 0; i < num_repetitions; i++)
    {
        memcpy((void *)local_test_data_buffer, (void *)global_test_data_buffer, data_size * sizeof(uint8_t));
    }
}

void test_serial_global_memory_to_slave_bandwidth(device_info_t *info)
{
    printf("Test serial reading from global to local memory\n");
    bandwidth_measurement_t m;
    int size_step = (GLOBAL_TEST_DATA_BUFFER_SIZE - 1) / NUM_POINTS_IN_MEASUREMENT;
    m = make_standard_measurement(test_serial_global_memory_to_slave_bandwidth_slave,
                                  size_step,
                                  NUM_POINTS_IN_MEASUREMENT,
                                  100);
    print_measurement(m);
    info->global_memory_to_slave_bandwidth_byte_per_s = m.bandwidth;
    info->global_memory_to_slave_latency_ns = m.latency_ns;
}


void test_slave_to_global_memory_bandwidth_slave(int data_size, int num_repetitions)
{
    volatile uint8_t local_test_data_buffer[GLOBAL_TEST_DATA_BUFFER_SIZE] __attribute__ ((aligned (16)));
    for (int i = 0; i < num_repetitions; i++)
    {
        malt_memcpy((void *)global_test_data_buffer, (void *)local_test_data_buffer, data_size * sizeof(uint8_t));
    }
}

void test_slave_to_global_memory_bandwidth(device_info_t *info)
{
    printf("Test block writing from local to global memory\n");
    bandwidth_measurement_t m;
    int size_step = (GLOBAL_TEST_DATA_BUFFER_SIZE - 1) / NUM_POINTS_IN_MEASUREMENT;
    m = make_standard_measurement(test_slave_to_global_memory_bandwidth_slave,
                                  size_step,
                                  NUM_POINTS_IN_MEASUREMENT,
                                  NUM_REPETITIONS);
    print_measurement(m);
    info->slave_to_global_memory_block_bandwidth_byte_per_s = m.bandwidth;
    info->slave_to_global_memory_block_latency_ns = m.latency_ns;
}


void test_serial_slave_to_global_memory_bandwidth_slave(int data_size, int num_repetitions)
{
    volatile uint8_t local_test_data_buffer[GLOBAL_TEST_DATA_BUFFER_SIZE] __attribute__ ((aligned (16)));
    
    for (int i = 0; i < num_repetitions; i++)
    {
        memcpy((void *)global_test_data_buffer, (void *)local_test_data_buffer, data_size * sizeof(uint8_t));
    }
}

void test_serial_slave_to_global_memory_bandwidth(device_info_t *info)
{
    printf("Test serial writing from local to global memory\n");
    bandwidth_measurement_t m;
    int size_step = (GLOBAL_TEST_DATA_BUFFER_SIZE - 1) / NUM_POINTS_IN_MEASUREMENT;
    m = make_standard_measurement(test_serial_slave_to_global_memory_bandwidth_slave,
                                  size_step,
                                  NUM_POINTS_IN_MEASUREMENT,
                                  100);
    print_measurement(m);
    info->slave_to_global_memory_bandwidth_byte_per_s = m.bandwidth;
    info->slave_to_global_memory_latency_ns = m.latency_ns;
}


void test_slave_to_progressor_edge_slave(int data_size, int num_repetitions)
{
    volatile uint8_t local_test_data_buffer[GLOBAL_TEST_DATA_BUFFER_SIZE] __attribute__ ((aligned (16)));
    const uint32_t offset = 0;
    const uint32_t row = 0;
    for (int i = 0; i < num_repetitions; i++)
    {
		#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
        pgr_mem_ext_write_row_edge_data(offset,
                                        (void*)local_test_data_buffer,
                                        data_size,
                                        LEFT_EDGE_MEM,
                                        row);
		#pragma GCC diagnostic pop
    }
}

void test_slave_to_progressor_edge_memory_bandwidth(device_info_t *info)
{
    printf("Test slave to progressor edge writing\n");
    bandwidth_measurement_t m;
    int size_step = (GLOBAL_TEST_DATA_BUFFER_SIZE - 1) / NUM_POINTS_IN_MEASUREMENT;
    m = make_standard_measurement(test_slave_to_progressor_edge_slave,
                                  size_step,
                                  NUM_POINTS_IN_MEASUREMENT,
                                  100);
    print_measurement(m);
    info->slave_to_coprocessor_bandwidth_byte_per_s = m.bandwidth;
    info->slave_to_coprocessor_latency_ns = m.latency_ns;
}


void test_progressor_edge_to_slave_slave(int data_size, int num_repetitions)
{
    volatile uint8_t local_test_data_buffer[GLOBAL_TEST_DATA_BUFFER_SIZE] __attribute__ ((aligned (16)));
    const uint32_t offset = 0;
    const uint32_t row = 0;
    for (int i = 0; i < num_repetitions; i++)
    {
        pgr_mem_ext_read_row_edge_data((void*)local_test_data_buffer,
                                        offset,
                                        data_size,
                                        LEFT_EDGE_MEM,
                                        row);
    }
}

void test_progressor_edge_to_slave_memory_bandwidth(device_info_t *info)
{
    printf("Test progressor edge to slave reading\n");
    bandwidth_measurement_t m;
    int size_step = (GLOBAL_TEST_DATA_BUFFER_SIZE - 1) / NUM_POINTS_IN_MEASUREMENT;
    m = make_standard_measurement(test_progressor_edge_to_slave_slave,
                                  size_step,
                                  NUM_POINTS_IN_MEASUREMENT,
                                  100);
    print_measurement(m);
    info->coprocessor_to_slave_bandwidth_byte_per_s = m.bandwidth;
    info->coprocessor_to_slave_latency_ns = m.latency_ns;
}

void test_progressor_memory_shift_slave(const int data_size, const int num_repetitions)
{
    for (int i = 0; i < num_repetitions; i++)
    {
        pgr_mem_shift_right();
    }
}

void test_progressor_memory_shift(device_info_t *info)
{
    printf("Test progressor memory shift\n");
    bandwidth_measurement_t m;
    int size_step = 1;
    m = make_standard_measurement(test_progressor_memory_shift_slave,
                                  size_step,
                                  1,
                                  100);
    print_measurement(m);
    info->coprocessor_shift_latency_ns = m.latency_ns;
}

/**********************   Main   **********************/

int main()
{
    malt_start_thr(fill_basic_device_info, &device_info);
    malt_sm_wait_all_lines_free();

    test_global_memory_to_slave_bandwidth(&device_info);
    test_serial_global_memory_to_slave_bandwidth(&device_info);
    test_slave_to_global_memory_bandwidth(&device_info);
    test_serial_slave_to_global_memory_bandwidth(&device_info);
    test_slave_to_progressor_edge_memory_bandwidth(&device_info);
    test_progressor_edge_to_slave_memory_bandwidth(&device_info);
    test_progressor_memory_shift(&device_info);

    print_device_info(&device_info);

    //mnk_test();    
    //return malt_local_stack_call(main_local);
    return 0;
}
