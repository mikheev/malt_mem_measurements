#ifndef __MEASUREMENTS_COMMON_H
#define __MEASUREMENTS_COMMON_H

#define MAX_NUM_POINTS_IN_MEASUREMENT     10


typedef struct{
    int number_points;
    char name[32];
    uint64_t time_ps[MAX_NUM_POINTS_IN_MEASUREMENT];
    uint64_t data_size[MAX_NUM_POINTS_IN_MEASUREMENT];
    uint64_t bandwidth;
    uint64_t latency_ns;
} bandwidth_measurement_t;


static inline void approximate_linear(const uint64_t* x_arr, const uint64_t* y_arr, const int num_points, uint64_t* k_ptr, uint64_t* b_ptr)
{
     uint64_t k = 1;
     uint64_t b = 2;
     
     uint64_t x_sum = 0;
     uint64_t y_sum = 0;
     uint64_t xy_sum = 0;
     uint64_t x_sqr_sum = 0;
     
     for (int i = 0; i < num_points; i++)
     {
          x_sum += x_arr[i];
          y_sum += y_arr[i];
          xy_sum += x_arr[i] * y_arr[i];
          x_sqr_sum += x_arr[i] * x_arr[i];
     }
     
     k = (num_points * xy_sum - x_sum * y_sum) / (num_points * x_sqr_sum - x_sum * x_sum);
     b = (y_sum - k * x_sum) / num_points;
     
     *k_ptr = k;
     *b_ptr = b;
     
     uint64_t k_alt = (y_arr[num_points - 1] - y_arr[0]) / (x_arr[num_points - 1] - x_arr[0]);
     
     // Verification
     if (((k_alt > 2 * k) || (k_alt < k / 2)) & (k > 5))
     {
         printf("Warning!!! Incorrect calculation. k = %lld, k_alt = %lld\n", k, k_alt);
     }
}


static inline void mnk_test(void)
{
    uint64_t x[4] = {0, 1, 2, 3};
    uint64_t y[4] = {1, 30, 50, 70};
    
    uint64_t k = 0;
    uint64_t b = 0;
    uint64_t n = 4;
    
    approximate_linear(x, y, n, &k, &b);
    
    printf("k = %lld, b = %lld\n", k, b);
}


static inline bandwidth_measurement_t make_standard_measurement(void (*slave_func)(int, int), int size_step, int num_points, int num_repetitions)
{
    bandwidth_measurement_t m;
    int data_size;
    uint64_t t_start_us, t_stop_us;

    m.number_points = 0;
    for (int i = 0; i < num_points; i++)
    {
        data_size = 1 + i * size_step;
        
        t_start_us = malt_get_time_us();
        malt_start_pcnt();
        
        malt_start_thr(slave_func, data_size, num_repetitions);
        malt_sm_wait_all_lines_free();
        
        malt_stop_pcnt();
        t_stop_us = malt_get_time_us();
        
        m.data_size[i] = data_size;
        m.time_ps[i] = ((t_stop_us - t_start_us) * 1000000) / num_repetitions;
        m.number_points++;
    }

    
    if (num_points > 1)
    {
        uint64_t k, b;
        approximate_linear(m.data_size, m.time_ps, m.number_points, &k, &b);
        printf("K: %lld\n", k);
        m.bandwidth = 1000000000 / k;  // mbyte / s
        m.latency_ns = b / 1000;
    }
    else
    {
        // Warning!!! We don't know mechanism: latency or bandwidth
        m.latency_ns = m.time_ps[0] / 1000;
        m.bandwidth = 0; 
    }

    return m;
}


static inline void print_measurement(bandwidth_measurement_t m)
{
    printf("bandwidth: %lld kbyte/s\n", m.bandwidth);
    printf("latency: %lld ns\n", m.latency_ns);
    
    printf("times_ps = [");
    for (int i = 0; i < m.number_points; i++)
    {
        printf("%lld, ", m.time_ps[i]);
    }
    printf("]\n");
    printf("data_sizes = [");
    for (int i = 0; i < m.number_points; i++)
    {
        printf("%lld, ", m.data_size[i]);
    }
    printf("]\n");
}

#endif // __MEASUREMENTS_COMMON_H
