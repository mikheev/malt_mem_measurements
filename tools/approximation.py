import numpy as np
import matplotlib.pyplot as plt

# Insert arrays from malt test application output here 
times_ps = [2770000, 3860000, 6920000, 6920000, 6920000, 6920000, 9940000, 5910000, 5930000, 5940000, ]
data_sizes = [1, 100, 199, 298, 397, 496, 595, 694, 793, 892, ]

times_us = np.array(times_ps) / 1000000
data_sizes = np.array(data_sizes)


def mnk_fit(x, y):
    n = len(x)
    k = (n * np.dot(x, y) - np.sum(x) * np.sum(y)) / (n * np.sum(x**2) - np.sum(x)**2)
    b = (np.sum(y) - k * np.sum(x)) / n
    return (k, b)

k, b = mnk_fit(data_sizes, times_us)

plt.title("lat = {:.2f} мкс, bw = {:.2f} мбайт / с".format(b, 1/k))
plt.plot(data_sizes, times_us, marker="o", linestyle="None")
plt.plot(data_sizes, k * data_sizes + b, linestyle="dashed")
plt.axhline(b, linestyle="dashed")
plt.xlabel("Объём данных, байт")
plt.ylabel("Время, мкс")
plt.xlim(0)
plt.ylim(0)
plt.show()