#include <stdio.h>
#include <stdint.h>
#include <libmalt.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include <progressor/malt_progressor.h>

#include "../common/measurements_common.h"


#define GLOBAL_TEST_DATA_BUFFER_SIZE  1000
#define NUM_POINTS_IN_MEASUREMENT     10
#define NUM_REPETITIONS               100

// Buffer for testing data exchange with global memory
volatile uint8_t global_test_data_buffer[GLOBAL_TEST_DATA_BUFFER_SIZE];

/**********************   Slave functions   **********************/

// Execution time of this function will be measured.
void slave_test_function(int data_size, int num_repetitions)
{
    volatile uint8_t local_test_data_buffer[GLOBAL_TEST_DATA_BUFFER_SIZE];
    for (int i = 0; i < num_repetitions; i++)
    {
        malt_memcpy((void *)local_test_data_buffer, (void *)global_test_data_buffer, data_size * sizeof(uint8_t));
    }
}

/**********************   Master functions   **********************/

void make_single_measurement(void)
{
    const uint32_t data_size = 1000; // Data to transfer in bytes

    uint64_t t_start_us, t_stop_us;    
    
    t_start_us = malt_get_time_us();
    malt_start_pcnt();
    
    malt_start_thr(slave_test_function, data_size, NUM_REPETITIONS);
    malt_sm_wait_all_lines_free();
    
    malt_stop_pcnt();
    t_stop_us = malt_get_time_us();

    printf("--- Simple test ---\n");
    printf("Bytes transferred: %d\n", data_size);
    printf("Number repetitions: %d\n", NUM_REPETITIONS);
    printf("Time for a single iteration: %lld ps\n", (t_stop_us - t_start_us) * 1000000 / NUM_REPETITIONS);

}

// Make series of measurements with different data sizes.
// Approximate results with linear function and evaluate latency and bandwidth.
void perform_standard_test()
{
    printf("--- Standard_test ---\n");
    bandwidth_measurement_t m;
    int size_step = (GLOBAL_TEST_DATA_BUFFER_SIZE - 1) / NUM_POINTS_IN_MEASUREMENT;
    m = make_standard_measurement(slave_test_function,
                                  size_step,
                                  NUM_POINTS_IN_MEASUREMENT,
                                  NUM_REPETITIONS);
    print_measurement(m);
}

/**********************   Main   **********************/

int main()
{
    make_single_measurement();
    perform_standard_test();
    return 0;
}
